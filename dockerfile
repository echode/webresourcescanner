FROM alpine:3.19
ADD src/* /app/
ENV VIRTUAL_ENV=/opt/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN <<EOR
  adduser -D www
  apk add --no-cache py3-pip chromium-chromedriver
  python3 -m venv $VIRTUAL_ENV
  pip install -r /app/requirements.txt
EOR
USER www
WORKDIR /app
ENTRYPOINT ["gunicorn", "webresourcescanner:server", "-b", "0.0.0.0:8000", "-w"]
CMD [ "4" ]
EXPOSE 8000
