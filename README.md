# Web Resource Scanner

API that launches a given url in a headles Chromium instance and returns a json object containing a list of the different domain names accessed and a list of all requests containing url, content-type, status and response body size.

## Running in Docker

This is the easiest way to get things up and running

### Build

Make sure you have docker and buildx installed, then run ``docker build . --tag wrs:latest``.

### Run

For testing purposes, run ``docker run --name wrs-temp --publish 8000:8000 --rm wrs:latest``.

In production you should probably configure the amount of workers and put the endpoint behind a reverse proxy.

To run the container in the background, run

```
docker network create wrs
docker run --name wrs --network wrs --publish 127.0.0.1:8000:8000 --restart unless-stopped --detach wrs:latest 16
```

The number at the end is the amount of workers. This should probably be between 2-4 times the number of CPU cores on the server. (default is 4)

## Running standalone

You can also run the server directly on most linux distros.

### Installing dependencies

Make sure you have python3.4 or later and pip installed on your system, then run

```
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

You also need Chrome/Chromium and ChromeDriver in your $PATH.

If you can't find these in your distros package manager, you can download them from [GitHub](https://googlechromelabs.github.io/chrome-for-testing/)

### Run

For testing, you can run ``flask --app webresourcescanner run --debug`` and the server will listen localhost port 5000 with debug mode enabled.

In production we need to run the app through a WSGI server.

There are many WSGI servers to choose from, but for simplicity this project includes [Gunicorn](https://gunicorn.org) in its dependencies.

To start the server using Gunicorn, you can run

```
source .venv/bin/activate
gunicorn webresourcescanner:server --bind=0.0.0.0:8000 --workers=16
```

The number of workers should probably be between 2-4 times the number of CPU cores on the server.

You should probably put the application behind a reverse proxy to protect it with ssl and maybe authentication

## Usage

Just send a post request to the server, where the body contains the url you want to scan.

Example using curl (and python to make the json output look pretty):

```
user@machine:~/webresourcescanner# curl http://localhost:8000 --request POST --data 'https://www.github.com' --silent | python3 -m json.tool

{
    "self": [
        "www.github.com",
        "github.com"
    ],
    "external": [
        "github.githubassets.com"
    ],
    "requests": [
        {
            "url": "https://www.github.com/",
            "status": 301,
            "content-type": null,
            "size": 0
        },
        {
            "url": "https://github.com/",
            "status": 200,
            "content-type": "text/html; charset=utf-8",
            "size": 42929
        },
        {
            "url": "https://github.githubassets.com/assets/light-0eace2597ca3.css",
            "status": 200,
            "content-type": "text/css",
            "size": 4258
        },
        {
            "url": "https://github.githubassets.com/assets/dark-a167e256da9c.css",
            "status": 200,
            "content-type": "text/css",
            "size": 4293
        },
	...
    ]
}

```
