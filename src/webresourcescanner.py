from flask import Flask, request
from seleniumwire import webdriver # No longer maintained, possible replacement?: https://pypi.org/project/selenium-requests/ | https://www.rkengler.com/how-to-capture-network-traffic-when-scraping-with-selenium-and-python/
from selenium.webdriver.chrome.options import Options
from werkzeug.exceptions import HTTPException, BadRequest
from urllib.parse import urlparse
import json

### Functions ###

def fetch(url: str) -> list:
	resources = []
	chrome_options = Options()
	chrome_options.add_argument('--headless')
	chrome_options.add_argument('--incognito')
	chrome_options.add_argument('--no-sandbox')
	chrome_options.add_argument('--disable-dev-shm-usage')
	chrome_options.add_argument('--aggressive-cache-discard')
	chrome_options.add_argument('--ignore-certificate-errors')
	driver = webdriver.Chrome(options=chrome_options)
	try:
		driver.get(url)
		for request in driver.requests:
			if request.response and 'accounts.google.com/ListAccounts' not in request.url:
				resource = {
					'url': request.url,
					'status': request.response.status_code,
					'content-type': request.response.headers['Content-Type'],
                                        'size': len(request.response.body)
				}
				resources.append(resource)
	except:
		print(f'Something broke while fetching url {url}')
	finally:
		driver.quit()
	return resources

### Application ###

server = Flask(__name__)

@server.errorhandler(BadRequest)
def err_bad_request(error):
	return error, 400

@server.route('/', methods=['GET'])
def index():
	return '<p>Hello, World!</p>'

@server.route('/', methods=['POST'])
def domain():
	rlist = {
		'self': [],
		'external': [],
                'requests': []
	}
	if request.content_length > 256:
		raise BadRequest('Request to large')
	string = request.get_data(as_text=True)
	try:
		url = urlparse(string)
		base = url.hostname.split('.')[-2:]
	except:
		raise BadRequest('Failed to parse url')
	rlist['requests'] = fetch(string)
	for req in rlist['requests']:
		uri = urlparse(req['url'])
		if base == uri.hostname.split('.')[-2:] and uri.netloc not in rlist['self']:
			rlist['self'].append(uri.netloc)
		elif uri.netloc not in rlist['self'] and uri.netloc not in rlist['external']:
			rlist['external'].append(uri.netloc)
	return json.dumps(rlist)
